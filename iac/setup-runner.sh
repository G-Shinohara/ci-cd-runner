#!/bin/bash

while getopts t: flag; do
  case "${flag}" in
  t) registration_token=${OPTARG} ;;
  esac
done

# Install runner

apt update -y
apt upgrade -y

curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash

apt-get install gitlab-runner -y

gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token $registration_token \
  --description "gcp, cloud, linux" \
  --tag-list "gcp,cloud,linux" \
  --executor "shell"

gitlab-runner start

# Install docker

apt-get update
apt-get install ca-certificates curl gnupg

install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
chmod a+r /etc/apt/keyrings/docker.gpg

echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" |
  sudo tee /etc/apt/sources.list.d/docker.list >/dev/null

apt-get update

apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

usermod -aG docker gitlab-runner
